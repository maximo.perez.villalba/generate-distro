<?php
require_once '../vendor/autoload.php';

use codemax\distro\Distro;

$distro = new Distro( __DIR__.'/generate-distro.json' );

$distro->generate();


